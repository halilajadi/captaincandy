import { Fragment } from "react";

const faqData = {
    title: {
        en: "Frequently asked questions",
        nl: "Veelgestelde vragen",
    },
    en: [
    	{
            id: "1",
            title: " Where do the sweets in Captain Candy’s stores come from?", 
            content: "Captain Candy’s ships travel all over Europe (from Spain to Germany, from Sweden to Italy) to the very best suppliers for candy. All you have to do is look at the barrel or the box and that will tell you where the particular candy has sailed from."
        },
        {
            id: "2",
            title: " What is the shelf life of the candy?", 
            content: "As we pirates say, “You can find the answer to everything in a barrel”. In this case not actually in the barrel, but on the barrel. The candy’s shelf life is written on each of them.",
        },
        {
            id: "3",
            title: "How should the candy be stored after it has been bought?", 
            content: "In the same way as supplies on pirate expeditions - if possible in dry and cold conditions",
        },
        {
            id: "4",
            title: "Is it okay that the candy in the stores lies in open barrels?", 
            content: "Although pirates often lived outside the law, everything’s all right here. It’s called “free sale” and doesn’t break hygiene rules in any way. It’s like when you put bread in a bag in a port’s “super” market…",
        },
        {
            id: "5",
            title: "Where does the Captain Candy company come from?", 
            content: "We are a Czech company although we have docked with Captain Candy in other countries too.",
        },
        {
            id: "6",
            title: "Why can’t we try the candy in the barrels?", 
            content: "Unfortunately, it’s not possible for hygiene reasons. But don’t be afraid to ask our crews in the stores – they will be happy to help you find the exact taste you are looking for.",
        },
        {
            id: "7",
            title: "Is it hygienic to put tongs on barrels? ", 
            content: "The old pirate rule states that “tongs belong on a barrel”. What’s more, here too we respect the law which doesn’t see any problem with this.",
        },
        {
            id: "8",
            title: "How many stores does Captain Candy have?", 
            content: "So far we have dropped anchor in four places in Prague and we have two harbours each in Dubrovnik in Croatia and in Venice in Italy. And we are planning more expeditions!",
        },
        {
            id: "9",
            title: "Does the Captain Candy also work as a franchise?", 
            content: "You can bet your life on it! Captain Candy will soon be available as a franchise.",
        },
        {
            id: "10",
            title: "Are the barrels really completely full?", 
            content: "Pirates must have some secrets. And that’s why we’ll leave it to you to guess.",
        },
        {
            id: "11",
            title: "Which is the most popular candy?", 
            content: "Not even the famous sorceress from the Island of Crystal Balls could answer that. Everyone chooses whatever suits their taste and the best-selling candy often changes. If you look at the barrels in the stores you will find out more.",
        },
        {
            id: "12",
            title: "What are the stores’ opening hours?", 
            content: <Fragment>From Monday to Friday we take in every traveller from 10 in the morning till 11 at night. At weekends between 10 o’clock in the morning and midnight. You can look in detail here. <a href="/stores">here</a>.</Fragment>,
        },
        {
            id: "13",
            title: " Why isn’t the price of the candy written on each barrel?", 
            content: " Because there’s no need. ;) The price per 100 g is uniform for the entire selection and you will always find it written in the store.",
        },
    ], nl: [
    	{
            id: "1",
            title: "Waar komen de snoepjes in Captain Candy winkels vandaan?", 
            content: "De schepen van Captain Candy halen de snoepjes bij de beste leveranciers door heel Europa (van Spanje tot Duitsland, van Zweden tot Italië). Één blik op het vat of de doos en je weet al waar het snoep vandaan komt.",
        },
    	{
            id: "2",
            title: "Wat is de houdbaarheid van de snoep?", 
            content: "Zoals wij piraten zeggen: “Antwoord op al je vragen vind je in het vat.” In dit geval niet in het vat maar op het vat. Op elk vat staat de houdbaarheidsdatum van de snoepjes vermeld.",
        },
    	{
            id: "3",
            title: "Hoe moeten snoepjes na aankoop worden bewaard?", 
            content: "Zoals voorraad tijdens piratenexpedities - bij voorkeur droog en koud.",
        },
    	{
            id: "4",
            title: "Mag snoep in winkels op open vaten liggen?", 
            content: 'Hoewel piraten vaak buiten de wet staan, is dit helemaal in orde. Het gaat hier om "de vrije verkoop" en deze is op geen enkele manier in strijd met de hygiënische normen. Het is vergelijkbaar met het kopen van brood op de havenmarkt...',
        },
    	{
            id: "5",
            title: "Waar komt het bedrijf Captain Candy vandaan?", 
            content: "We zijn een Tsjechisch bedrijf maar we zijn met Captain Candy al in meerdere landen aangemeerd.",
        },
    	{
            id: "6",
            title: "Waarom mag ik geen snoepjes uit de vaten proeven?", 
            content: "Helaas is dit om hygiënische redenen niet mogelijk. Maar wees niet bang om onze bemanning in de winkels te vragen - ze helpen je graag de juiste smaak te vinden die je zoekt.",
        },
    	{
            id: "7",
            title: "Is het hygiënisch om de tang op het vat te leggen?", 
            content: "Zoals de oude piratenregel zegt: &quot;De tang hoort op een vat te liggen”. Bovendien respecteren we hiermee de wet die in deze handeling geen probleem ziet.",
        },
    	{
            id: "8",
            title: "Hoeveel winkels heeft Captain Candy?", 
            content: "Tot nu toe staan we verankerd op verschillende plaatsen in Praag, in Venetië en Verona. Onze nieuwste haven is in Amsterdam. En we bereiden nog meer expedities voor!",
        },
    	{
            id: "9",
            title: "Werkt het Captain Candy-concept als franchise?", 
            content: "Zeker weten! Captain Candy kan binnenkort als franchise worden opgezet. Mocht je interesse hebben in franchising, meer informatie vind je hier.",
        },
    	{
            id: "10",
            title: "Zijn de vaten echt helemaal vol?", 
            content: "Piraten moeten ook een aantal geheimen hebben. Dit willen we even in het middel laten ;)",
        },
    	{
            id: "11",
            title: "Welk snoepje is het populairst?", 
            content: "Zelfs de bekende tovenaar van het Crystal Ball Island kan daar geen antwoord op geven. Iedereen kiest volgens zijn/haar smaak en het best verkochte snoepje verandert vaak door het jaar heen. Meer informatie vind je op de vaten in de winkel.",
        },
    	{
            id: "12",
            title: "Wat zijn de openingstijden van de winkels?", 
            content: "Elke winkel hanteert zijn eigen piratenregels. Meer details vind je hier.",
        },
    	{
            id: "13",
            title: "Waarom staat er geen prijs op elk vat?", 
            content: "Omdat het niet nodig is ;) De prijs per 100 gram is gelijk voor het hele assortiment en staat altijd vermeld in de winkel.",
        },
    ]
};

const dataProcessing = {
    en: {
        title: "Personal data processing",
        parag: [
            'Al fine di proteggere i dati personali, acconsento, in qualità di parte interessata dei dati personali, alla raccolta, conservazione e trattamento dei seguenti dati personali: nome, cognome, indirizzo di posta elettronica, indirizzo di residenza, data di nascita, numero del telefono cellulare, informazioni sul titolo di studio e altre informazioni volontariamente fornite nel questionario inviato, da parte del titolare dei dati personali, ossia: Tresher s.r.o., sede legale: Praga - Staré Město, V kotcích 526/1, CAP 110 00, codice fiscale 24217280, partita IVA CZ24217280, registrata presso il Tribunale di Praga, atti n. 189598 C ("titolare").',
            "This consent is granted for an indefinite period until the subject withdraws his/her consent.",
            "Lo scopo del trattamento dei dati personali dell'interessato stabilito dal titolare è quello di utilizzare i dati personali solo per l'eventuale instaurazione di una cooperazione commerciale con l'interessato.",
            "Il titolare dichiara che raccoglierà i dati personali nella misura necessaria per l'adempimento delle finalità sopra indicate e che li tratterà solo in conformità con lo scopo per cui sono stati raccolti. Il titolare dichiara che i dati personali saranno trattati nel modo seguente: a) meccanicamente (automaticamente), via computer e software nell'ambito del sistema informatico; b) manualmente.",
            "Inoltre, dichiaro di essere stato informato e di conoscere l'informativa sulla privacy del titolare.",
        ]
    },
    nl: {
        title: "Verwerking van persoonsgegevens",
        parag: [
            'Al fijne bescherming tegen persoonlijke gegevens, acconsento, in qualità di parte interessata dei personali, alla raccolta, conservazione and trattamento dei seguenti thati personali: nome, cognome, indirizzo di posta electronica, indirizzo di residenza, data di nascita, numero del mobiele telefoons, informatie over studio-titels en andere informatie-informatie voor meerdere vragen, het deel van de titolare dei dati personali, ossia: Tresher s.r.o., sede legale: Praga - Staré Město, V kotcích 526/1, CAP 110 00, codice fiscale 24217280, partita IVA CZ24217280, drukregister bij de rechtbank van Praag, att. 189598 C ("titel").',
            "Deze toestemming wordt verleend voor onbepaalde tijd totdat de proefpersoon zijn/haar toestemming intrekt.",
            "Los scopo del trattamento dei personali dell'interessato stabilito dal titolare è quello di utilizzare i dati personali solo per l'eventuale instaurazione di a cooperazione commerciale con l'interessato.",
            "Il titolare dichiara che raccoglierà i dati personali nella misura necessaria per l'adempimento delle finalità sopra aangeven e ch li tratterà solo conformità con lo scopo per cui sono stati raccolti. Il titolare dichiara che i dati personali saranno trattati nel modo seguente: a ) meccanicamente (automaticamente), via computer en software nell'ambito del sistema informatico; b) handmatig.",
            "Inoltre, dichiaro di essere stato informato e di conoscere l'informativa sulla privacy del titolare.",
        ]
    },
}

export {
    faqData,
    dataProcessing,
};