import logoEn from "../img/icons/en.jpg";
import logoNl from "../imgnl/nl.png";

const firstMenu = {
    en: [
        {
            id: "products",
            path: "/products",
            name: "Products",
        },
        {
            id: "captain",
            path: "/captain-story",
            name: "The Captain’s story",
        },
    ],
    nl: [
        {
            id: "products",
            path: "/products",
            name: "Producten",
        },
        {
            id: "captain",
            path: "/captain-story",
            name: "Kapitein verhaal",
        },
    ]
};

const secondMenu = {
    en: [
        {
            id: "about",
            path: "/about",
            name: "About us",
        },
        {
            id: "stores",
            path: "/stores",
            name: "Stores",
        },
    ],
    nl: [
        {
            id: "about",
            path: "/about",
            name: "Over ons",
        },
        {
            id: "stores",
            path: "/stores",
            name: "Onze winkels",
        },
    ]
};

const footerMenu = {
    en: [
        {
            id: "career",
            path: "/career",
            name: "Career",
        },
        {
            id: "faq",
            path: "/faq",
            name: "FAQ",
        },
        {
            id: "franchising",
            path: "/franchising",
            name: "Franchising",
        },
        {
            id: "contact",
            path: "/contact",
            name: "Contact",
        },
    ],
    nl: [
        {
            id: "career",
            path: "/career",
            name: "Carrière",
        },
        {
            id: "faq",
            path: "/faq",
            name: "FAQ",
        },
        {
            id: "franchising",
            path: "/franchising",
            name: "Franchising",
        },
        {
            id: "contact",
            path: "/contact",
            name: "Contact",
        },
    ]
};

const language = {
    nl: {
        code: "en",
        name: "English",
        img: logoEn,
    },
    en: {
        code: "nl",
        name: "Dutch",
        img: logoNl,
    },
};


export {
    firstMenu,
    secondMenu,
    language,
    footerMenu,
};