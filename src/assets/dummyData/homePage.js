import { Fragment } from "react";
import hpAboutImg from "../img/hp_about_1_inner-min.jpg";
import hpAboutInner from "../img/hp_about_2_inner-min.jpg";

const homePageData = {
    en: [
        {
            id: "123",
            img: hpAboutImg,
            title: "Find your treasure",
            description: "Raise the anchor of your imagination and set sail with us to discover the taste of eternal youth. Each of our candies conceals a different story!",
            buttonName: <Fragment>Explore candy <span>islands</span></Fragment>,
        },
        {
            id: "124",
            img: hpAboutInner,
            title: "I, Captain Candy",
            description: <Fragment>Discover a story full of adventure,<br />suspense and knowledge</Fragment>,
            buttonName: <Fragment>My story</Fragment>,
        },
    ],
    nl: [
        {
            id: "123",
            img: hpAboutImg,
            title: "Zoek je schat",
            description: "Licht het anker van je verbeelding en zet koers met ons om de smaken van eeuwige jeugd te ontdekken. Elk van onze snoepjes heeft een uniek verhaal!",
            buttonName: <Fragment>Verken de<span>snoepeilandjes</span></Fragment>,
        },
        {
            id: "124",
            img: hpAboutInner,
            title: "Ik, Captain Candy",
            description: <Fragment>Ontdek een verhaal vol avontuur, <br />spanning en kennis</Fragment>,
            buttonName: <Fragment>Mijn verhaal</Fragment>,
        },
    ]
};

export {
    homePageData
};