import aboutMin from "../img/about/1-min.png";
import aboutMin2 from "../img/about/2-min.png";
import aboutMin3 from "../img/about/3-min.png";
import aboutMin4 from "../img/about/4-min.png";
import aboutMin5 from "../img/about/5-min.png";
import aboutMin6 from "../img/about/6-min.png";
import aboutMin7 from "../img/about/7-min.png";
import aboutMin8 from "../img/about/8-min.png";
import aboutMin9 from "../img/about/9-min.png";
import about1 from "../img/about/1.svg";
import about2 from "../img/about/2.svg";
import about3 from "../img/about/3.svg";
import about4 from "../img/about/4.svg";
import about5 from "../img/about/5.svg";
import about6 from "../img/about/6.svg";
import about7 from "../img/about/7.svg";
import about8 from "../img/about/8.svg";
import about9 from "../img/about/9.svg";
// import about10 from "../img/about/33.png";
// import about11 from "../img/about/56.png";
import year2009 from "../img/about/2009.svg";
import year2010 from "../img/about/2010.svg";
// import year2011 from "../img/about/2011.svg";
import year2012 from "../img/about/2012.svg";
import year2013 from "../img/about/2013.svg";
import year2014 from "../img/about/2014.svg";
// import year2015 from "../img/about/2015.svg";
import year2016 from "../img/about/2016.svg";
import year2017 from "../img/about/2017.svg";
import year2020 from "../img/about/2020.svg";
import year2021 from "../img/about/2021.svg";
import { Fragment } from "react";

const AboutData = {
    footerText: {
        en: " …the Captain’s story continues!",
        nl: <Fragment>…het verhaal van de captain<br className='hidden-xs' /> gaat nog steeds door!</Fragment>,
    },
    en: [
        {
            img: aboutMin,
            yearImg: year2009,
            year: "2009",
            title: <Fragment>The first lines <br className='hidden-xs' />in&nbsp;the logbook</Fragment>,
            description: "The idea was born to bring the best flavours to everyone and to bring back memories of childhood at least for a little while.",
        },
        {
            img: aboutMin2,
            yearImg: year2010,
            year: "2010",
            title: <Fragment>We start  <br className="hidden-xs" />ploughing the seas</Fragment>,
            description: "In all the harbours we look for merchants who can fill our barrels with the best treasures on offer.",
        },
        // {
        //     img: aboutMin3,
        //     yearImg: year2011,
        //     year: "2011",
        //     title: <Fragment>We have found  <br className="hidden-xs" /> our treasure</Fragment>,
        //     description: " Our barrels are finally full and our desire to share all these tastes can finally be fulfilled. Captain Candy has arrived!",
        // },
        {
            img: aboutMin4,
            yearImg: year2012,
            year: "2012",
            title: <Fragment> We launch <br className="hidden-xs" /> our first ship on the water</Fragment>,
            description: "The very first ship in Captain Candy’s flotilla finds a place to anchor in Melantrichova Street in the centre of Prague",
        },
        {
            img: aboutMin5,
            yearImg: year2013,
            year: "2013",
            title: <Fragment>We leave <br className="hidden-xs" />  our territorial waters</Fragment>,
            description: "For the first time, one of our ships leaves our home port and anchors in one of the most beautiful port cities in the world – Dubrovnik in Croatia.",
        },
        {
            img: aboutMin6,
            yearImg: year2014,
            year: "2014",
            title: <Fragment>Our conques <br className="hidden-xs" /> continues</Fragment>,
            description: "We do well in the Croatian waters and two more stores see the light of day in Dubrovnik!",
        },
        // {
        //     img: aboutMin7,
        //     yearImg: year2015,
        //     year: "2015",
        //     title: <Fragment> We are not idle <br className="hidden-xs" /> at home either</Fragment>,
        //     description: "TWe expand our home port and anchor more ships in the coves of Old Prague, in Karlova and Havelská streets.",
        // },
        {
            img: aboutMin8,
            yearImg: year2016,
            year: "2016",
            title: <Fragment>To the Moats!</Fragment>,
            description: "After oceans, seas, bays, deltas and rivers we also start ploughing through Moats! And so we open our fourth store in Prague on Na Příkopě Street (příkop = moat in Czech!)",
        },
        {
            img: aboutMin9,
            yearImg: year2017,
            year: "2017",
            title: <Fragment>We continue to <br className="hidden-xs" />colonise</Fragment>,
            description: "A fully equipped ship with an experienced crew finds anchor in two stores in the Italian city of Venice..",
        },
        {
            img: aboutMin,
            yearImg: year2020,
            year: "2020",
            title: <Fragment>We are expanding<br className="hidden-xs" />to the Netherlands</Fragment>,
            description: "msterdam is our next stop on the way to the conquest of Europe. Our sweets make more and more people happy.",
        },
        {
            img: aboutMin7,
            yearImg: year2021,
            year: "2021",
            title: <Fragment>We have more <br className="hidden-xs" />countries in our sights</Fragment>,
            description: "Our plans are clear, we are sailing to Germany and it certainly won't be our last stop...",
        },
    ],
    nl: [
        {
            img: aboutMin,
            yearImg: year2009,
            year: "2009",
            title: <Fragment>De eerste alinea's <br className='hidden-xs' /> in het logboek</Fragment>,
            description: "Het idee was geboren om de beste smaken over de hele wereld te zoeken en deze naar mensen te brengen, zodat ze in hun herinneringen naar hun kindertijd konden terugkeren.",
        },
        {
            img: aboutMin2,
            yearImg: year2010,
            year: "2010",
            title: <Fragment>Onze eerste <br className="hidden-xs" /> expedities</Fragment>,
            description: "We bezoeken alle uithoeken van de wereld en zoeken handelaren die de beste snoepjes in de gekste designs produceren.",
        },
        {
            img: aboutMin3,
            yearImg: year2012,
            year: "2012",
            title: <Fragment>We zetten de laatste <br className="hidden-xs" />  puntjes op de i</Fragment>,
            description: "Zodat het uitzoeken en de aankoop van snoep bij ons een unieke ervaring is, bedenken we een buitengewone inrichting van de winkels die op het eerste gezicht in het oog springt.",
        },
        {
            img: aboutMin4,
            yearImg: year2013,
            year: "2013",
            title: <Fragment> De magazijnen <br className="hidden-xs" /> worden gevuld</Fragment>,
            description: "Onze voorraad is steeds groter totdat onze magazijnen eindelijk vol zijn en wij zijn klaar om onze zoete schatten met klanten te delen.",
        },
        {
            img: aboutMin4,
            yearImg: year2014,
            year: "2014",
            title: <Fragment>We gaan open</Fragment>,
            description: "De eerste winkel wordt feestelijk geopend in het hart van de oude binnenstad van Praag, in de Melantrichova straat.",
        },
        {
            img: aboutMin4,
            yearImg: year2016,
            year: "2016",
            title: <Fragment>Expeditie naar  <br className="hidden-xs" /> onbekende wateren.</Fragment>,
            description: "Voor de eerste keer ooit verlaten we het thuisland en varen we naar de onbekende wateren van Dubrovnik in Kroatië, een van de mooiste Europese havens.",
        },
        {
            img: aboutMin4,
            yearImg: year2017,
            year: "2017",
            title: <Fragment>We veroveren de  <br className="hidden-xs" /> Middellandse Zee</Fragment>,
            description: "Vanaf nu is Italië niet alleen de zee en de zon. Sinds kort kunnen toeristen onze snoepjes in verschillende winkels bewonderen!",
        },
        {
            img: aboutMin4,
            yearImg: year2020,
            year: "2020",
            title: <Fragment>We breiden uit naar Nederland</Fragment>,
            description: "Amsterdam is onze volgende stop op weg naar de verovering van Europa. Onze snoepjes maken meer en meer mensen blij.",
        },
        {
            img: aboutMin7,
            yearImg: year2021,
            year: "2021",
            title: <Fragment>We hebben meer<br className="hidden-xs" />landen in het vizier</Fragment>,
            description: "Onze plannen zijn duidelijk, we varen naar Duitsland en het is zeker niet onze laatste stop...",
        },
        // {
        //     img: aboutMin4,
        //     yearImg: year2017,
        //     year: "2017",
        //     title: <Fragment>We continue to <br className="hidden-xs" />colonise</Fragment>,
        //     description: "A fully equipped ship with an experienced crew finds anchor in two stores in the Italian city of Venice..",
        // },
        // {
        //     img: aboutMin4,
        //     yearImg: year2017,
        //     year: "2017",
        //     title: <Fragment>We continue to <br className="hidden-xs" />colonise</Fragment>,
        //     description: "A fully equipped ship with an experienced crew finds anchor in two stores in the Italian city of Venice..",
        // },
    ],
}

const routeImgData = [
    {
        step: "step1",
        imgAbout: about1,
    },
    {
        step: "step2",
        imgAbout: about2,
    },
    {
        step: "step3",
        imgAbout: about3,
    },
    {
        step: "step4",
        imgAbout: about4,
    },
    {
        step: "step5",
        imgAbout: about5,
    },
    {
        step: "step6",
        imgAbout: about6,
    },
    {
        step: "step7",
        imgAbout: about7,
    },
    {
        step: "step8",
        imgAbout: about8,
    },
    {
        step: "step9",
        imgAbout: about9,
    },
    // {
    //     step: "step10",
    //     imgAbout: about11,
    // },
    // {
    //     step: "step11",
    //     imgAbout: about9,
    // },
];

export {
    routeImgData,
    AboutData,
};