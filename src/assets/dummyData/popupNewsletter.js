const newsletter = {
    en: {
        title: "WANT TO BE THE FIRST?",
        subttl: "Back in the olden days, you couldn’t get hold of exclusive information without having to undergo torture, walk the plank or be subjected to any keelhauling. These days you just need to send us an e-mail and hey presto!",
        placeholder: "Enter your e-mail address",
        button: "Sing up",
        error: "Body of a thousand whales! This email address has already been entered by someone else! Enter another email address."
    },
    nl: {
        title: "WIL JE DE EERSTE ZIJN?",
        subttl: "Vroeger was het onmogelijk om exclusieve informatie te krijgen zonder gebruik te maken van marteling, ze aan de mast te hangen, of de persoon te kielhalen. Tegenwoordig kun je ons gewoon een e-mailtje sturen en voilà!",
        placeholder: "Voer je e-mailadres in",
        button: "Aanmelden",
        error: "Lichaam van duizend walvissen! Dit e-mailadres is al door iemand anders ingevoerd! Voer een ander e-mailadres in.",
    },
};

export {
    newsletter
};