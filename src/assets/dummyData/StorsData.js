import benatky_1 from "../img/stores/benatky_1-min.jpg";
import benatky_2 from "../img/stores/benatky_2-min.jpg";
import benatky_3 from "../img/stores/benatky_3-min.jpg";
import praha_1 from "../img/stores/praha_1-min.jpg";
import praha_2 from "../img/stores/praha_2-min.jpg";
import praha_3 from "../img/stores/praha_3-min.jpg";
import praha_4 from "../img/stores/praha_4-min.jpg";

const StorsData = {
    en: [
        {
            id: "venecie",
            title: "Our stores in Venice",
            places: [
                {
                    img: benatky_1,
                    title: "Salizada San Giovanni Grisostomo 5803",
                    link: "https://www.google.cz/maps/place/Salizada+S.+Giovanni+Grisostomo,+5803,+30121+Venezia+VE,+It%C3%A1lie/@45.4390141,12.3349899,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1dbee572bd1:0x87490efe04ea7997!8m2!3d45.4390141!4d12.3371786",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—22:00",
                        },
                    ]
                },
                {
                    img: benatky_2,
                    title: "San Marco 5037",
                    link: "https://www.google.cz/maps/place/S.+Marco,+4742-4743,+30100+Venezia+VE,+It%C3%A1lie/@45.4362713,12.3335094,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1da159153df:0x4a162b15fe2576a6!8m2!3d45.4362676!4d12.3356981",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—22:00",
                        },
                    ]
                },
                {
                    img: benatky_3,
                    title: "Castello 5577/5578",
                    link: "https://www.google.cz/maps/place/S.+Marco,+4742-4743,+30100+Venezia+VE,+It%C3%A1lie/@45.4362713,12.3335094,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1da159153df:0x4a162b15fe2576a6!8m2!3d45.4362676!4d12.3356981",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—22:00",
                        },
                    ]
                },
            ]
        },
        {
            id: "prague",
            title: "Our stores in Prague",
            places: [
                {
                    img: praha_1,
                    title: "Melantrichova 1",
                    link: "https://www.google.cz/maps/@50.0849641,14.4218191,3a,46y,267.6h,86.72t/data=!3m6!1e1!3m4!1sigM9Am3u-c0OrnLUoue8qw!2e0!7i7680!8i3840",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—23:00",
                        },
                    ]
                },
                {
                    img: praha_2,
                    title: "Uhelný trh 6",
                    link: "https://www.google.cz/maps/@50.0840785,14.420428,3a,62.8y,9.09h,82.38t/data=!3m6!1e1!3m4!1s-wa11mDz91pa3WLL4tt34A!2e0!7i13312!8i6656?shorturl=1",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—21:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—21:00",
                        },
                    ]
                },
                {
                    img: praha_3,
                    title: "Na Příkopě 10",
                    link: "https://www.google.cz/maps/@50.0849958,14.4246329,3a,27.5y,131.4h,86.89t/data=!3m7!1e1!3m5!1s1iiflEkORGYC6_qeUPLSXQ!2e0!6s%2F%2Fgeo2.ggpht.com%2Fcbk%3Fpanoid%3D1iiflEkORGYC6_qeUPLSXQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D234%26h%3D106%26yaw%3D121.17849%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?shorturl=1",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—23:00",
                        },
                    ]
                },
                {
                    img: praha_4,
                    title: "Karlova 7",
                    link: "https://www.google.cz/maps/@50.0859994,14.4172249,3a,63.7y,8.8h,90.02t/data=!3m7!1e1!3m5!1sY5L3WH5ja-AgRDS5SfW3wQ!2e0!6s%2F%2Fgeo3.ggpht.com%2Fcbk%3Fpanoid%3DY5L3WH5ja-AgRDS5SfW3wQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D234%26h%3D106%26yaw%3D359.39172%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?shorturl=1",
                    linkName: "Show on map",
                    table: [
                        {
                            open: "Monday - Thursday",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Friday - Saturday",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Sunday",
                            time: "10:00—23:00",
                        },
                    ]
                },
            ]
        },
    ],
    nl: [
        {
            id: "venecie",
            title: "Onze winkels in Venetië",
            places: [
                {
                    img: benatky_1,
                    title: "Salizada San Giovanni Grisostomo 5803",
                    link: "https://www.google.cz/maps/place/Salizada+S.+Giovanni+Grisostomo,+5803,+30121+Venezia+VE,+It%C3%A1lie/@45.4390141,12.3349899,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1dbee572bd1:0x87490efe04ea7997!8m2!3d45.4390141!4d12.3371786",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—22:00",
                        },
                    ]
                },
                {
                    img: benatky_2,
                    title: "San Marco 5037",
                    link: "https://www.google.cz/maps/place/S.+Marco,+4742-4743,+30100+Venezia+VE,+It%C3%A1lie/@45.4362713,12.3335094,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1da159153df:0x4a162b15fe2576a6!8m2!3d45.4362676!4d12.3356981",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—22:00",
                        },
                    ]
                },
                {
                    img: benatky_3,
                    title: "Castello 5577/5578",
                    link: "https://www.google.cz/maps/place/S.+Marco,+4742-4743,+30100+Venezia+VE,+It%C3%A1lie/@45.4362713,12.3335094,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1da159153df:0x4a162b15fe2576a6!8m2!3d45.4362676!4d12.3356981",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—22:00",
                        },
                    ]
                },
            ]
        },
        {
            id: "prague",
            title: "Our stores in Prague",
            places: [
                {
                    img: praha_1,
                    title: "Melantrichova 1",
                    link: "https://www.google.cz/maps/@50.0849641,14.4218191,3a,46y,267.6h,86.72t/data=!3m6!1e1!3m4!1sigM9Am3u-c0OrnLUoue8qw!2e0!7i7680!8i3840",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—23:00",
                        },
                    ]
                },
                {
                    img: praha_2,
                    title: "Uhelný trh 6",
                    link: "https://www.google.cz/maps/@50.0840785,14.420428,3a,62.8y,9.09h,82.38t/data=!3m6!1e1!3m4!1s-wa11mDz91pa3WLL4tt34A!2e0!7i13312!8i6656?shorturl=1",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—21:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—22:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—21:00",
                        },
                    ]
                },
                {
                    img: praha_3,
                    title: "Na Příkopě 10",
                    link: "https://www.google.cz/maps/@50.0849958,14.4246329,3a,27.5y,131.4h,86.89t/data=!3m7!1e1!3m5!1s1iiflEkORGYC6_qeUPLSXQ!2e0!6s%2F%2Fgeo2.ggpht.com%2Fcbk%3Fpanoid%3D1iiflEkORGYC6_qeUPLSXQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D234%26h%3D106%26yaw%3D121.17849%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?shorturl=1",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—23:00",
                        },
                    ]
                },
                {
                    img: praha_4,
                    title: "Karlova 7",
                    link: "https://www.google.cz/maps/@50.0859994,14.4172249,3a,63.7y,8.8h,90.02t/data=!3m7!1e1!3m5!1sY5L3WH5ja-AgRDS5SfW3wQ!2e0!6s%2F%2Fgeo3.ggpht.com%2Fcbk%3Fpanoid%3DY5L3WH5ja-AgRDS5SfW3wQ%26output%3Dthumbnail%26cb_client%3Dsearch.TACTILE.gps%26thumb%3D2%26w%3D234%26h%3D106%26yaw%3D359.39172%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?shorturl=1",
                    linkName: "Toon op kaart",
                    table: [
                        {
                            open: "Maandag - Donderdag",
                            time: "10:00—23:00",
                        },
                        {
                            open: "Vrijdag - Zaterdag",
                            time: "10:00—24:00",
                        },
                        {
                            open: "Zondag",
                            time: "10:00—23:00",
                        },
                    ]
                },
            ]
        },
    ],
};

export {
    StorsData,
}