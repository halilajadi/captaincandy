import { Fragment } from "react";
import img1 from "../img/products/1-xs-min.png"
import img2 from "../img/products/2-xs-min.png"
import img3 from "../img/products/3-xs-min.png"
import img4 from "../img/products/4-xs-min.png"
import img5 from "../img/products/5-xs-min.png"
import img6 from "../img/products/6-xs-min.png"

const productsItems = {
    headTitle: {
        en: <Fragment>Hundreds of shapes, even more flavours <br className="hidden-xs" /> There are thousands of flavours in the world. Everyone has the chance to discover their favourite!</Fragment>,
        nl: <Fragment>Honderden vormen en nog meer smaken <br className="hidden-xs" />  Er zijn duizenden smaken in de wereld. Iedereen krijgt de kans om hun favoriet te ontdekken!</Fragment>
    },
    headSubTitle: {
        en: "We believe everyone deserves to discover sweets they’ll want to devour. This is why we choose the very best ingredients and the most reliable suppliers. Take a peek inside our barrels!",
        nl: "We zijn van mening dat iedereen de kans verdient om de snoepjes te ontdekken die ze willen versmaden. Dat is de reden dat we de beste ingrediënten kiezen van de meest betrouwbare leveranciers. Kijk eens in onze tonnen!"
    },
    en: [
        {
            title: "Tangy jelly",
            description: "Not every pirate will admit to having a sweet tooth. This seriously tangy jelly is different!  Treat yourself to your favourite sweet while holding onto your hard-core image with its rock hard outside and sweet soft centre.",
            img: img1,
        },
        {
            title: "Sweet jelly",
            description: "Banana, strawberry, bear or a dual-coloured skull, take your pick. This is the king among sweets. On a number of islands we visited, the wealth of the future bride is judged by the size of her jelly dowry!",
            img: img2,
        },
        {
            title: "Batons",
            description: "The baton forms an integral part of all pirate expeditions. Not only does it taste great and have fantastic lasting power, but if necessary it can also double up as a sailing rope and can come in handy for tying up hostages.",
            img: img3,
        },
        {
            title:<Fragment>Marsh-<br />mallow</Fragment>,
            description: "Not every pirate will admit to having a sweet tooth. This seriously tangy jelly is different!  Treat yourself to your favourite sweet while holding onto your hard-core image with its rock hard outside and sweet soft centre.",
            img: img4,
        },
        {
            title: "Chocolate",
            description: "While every proper pirate is tough on the outside, but soft on the inside, you’ll find the exact opposite with these chocolate sweets. Beneath the softest chocolate shell we find crunchy almonds, hazelnuts, chunks of coconut and much more!",
            img: img5,
        },
        {
            title: "Chewing gum",
            description: "When not chewing coca leaves, pirates can be found chewing gum. But not the typical white variety. Even the lowest ranking sailor turns his nose up at that. Pirate chewing gum is colourful and tastes different every time!",
            img: img6,
        },
    ],
    nl: [
        {
            title: "Pittige jelly",
            description: "Niet iedere piraat wil toegeven dat ze een zoetekauw zijn. Deze ernstig pittige jelly is anders! Trakteer jezelf op je favoriete snoepje en behoud je imago als stoere zeebonk met zijn keiharde buitenkant en zoete zachte kern.",
            img: img1,
        },
        {
            title: "Zoete jelly",
            description: "Banaan, aardbei, beertjes of dubbelgekleurde schedel, kies maar. Dit is de koning onder de snoepjes. Op enkele eilanden die we hebben bezocht, wordt de rijkdom van een toekomstige bruid bepaald aan de hand van haar bruidsschat van jelly!",
            img: img2,
        },
        {
            title: "Staafjes",
            description: "Een staaf maakt integraal deel uit van alle piratenexpedities. Ze smaken niet niet alleen geweldig en je doet er fantastische lang mee. Maar als het nodig is, kun je ze ook gebruiken om de zeilen te hijsen en ze zijn handig als je gijzelaars moet vastbinden.",
            img: img3,
        },
        {
            title: "Marshmallow",
            description: "Was ons schip maar gemaakt van marshmallows! Dan werd ieder kanonschot dat onze vijanden op ons afvuren teruggekaatst of opgeslobberd. We hebben al geprobeerd om er een kleine sloep van de maken, maar het meeste materiaal verdween op mysterieuze wijze...",
            img: img4,
        },
        {
            title: "Chocolade",
            description: "Terwijl ieder degelijk piratenschip stevig is van buiten en zacht van binnen, ontdek je precies het tegenovergestelde met deze chocoladesnoepjes. Onder de allerzachtste schil van chocolade vind je knapperige amandelen, hazelnoten, stukjes kokos en nog veel meer!",
            img: img5,
        },
        {
            title: "Kauwgom",
            description: "Ale ze niet op cocabladeren kauwen, dan genieten piraten wel van kauwgom. Maar niet de normale witte variant. Zelfs de matroos met de laagste rang haalt er zijn neus voor op. Piratenkauwgom is kleurrijk en smaakt telkens anders!",
            img: img6,
        },
    ],
}

export { productsItems };