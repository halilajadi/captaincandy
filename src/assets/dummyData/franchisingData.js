import { Fragment } from "react";
import { Link } from "react-router-dom";
const toTop = () => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
};

const franchisingData = {
    en: {
        title: "Captain Candy Franchising",
        history: [
            {
                question: "Captain Candy concept caught your eye?",
                answer: "Captain Candy is a Czech candy shop selling a wide range of candies ready for devouring. The first store was opened in 2012 in Prague and since then Captain Candy has spread its wings all the way to Croatia and Italy. There are currently 10 stores in total.",
            },
            {
                question: "Keen to spread the joy of returning to being a kid to others around you?",
                answer: " Check out what you can gain from a Captain Candy franchise and get in touch. We can then talk through the rest in person.",
            },
        ],
        list: [
            {
                headingtitle: "Why work with us as a franchising partner?",
                headingLi: [
                    'Our concept boasts its own unique style',
                    "We offer quality of an international calibre",
                    "We’re constantly developing and improving",
                    "We’re a tried and tested European brand",
                ],
            },
            {
                headingtitle: "What we offer franchising partners?",
                headingLi: [
                    "The security of working for an international brand",
                    "Tried and tested sales know-how, which is constantly being innovated",
                    "Well-thought out, tried and tested and ready-made solutions",
                    "Help not only before opening your store and in the early stages, but also any time further down the line",
                    "The opportunity for personal development and self-realisation in getting your business off the ground",
                    "The chance to contribute to the development and growth of the brand",
                    "Stocking up with products and the necessary equipment to operate a franchise",
                    "System of regular checks and consultations",
                    "Long-term income and appreciation of your investment",
                    "Mutual agreement on a specific form of cooperation",
                ]
            }
        ],
        listParagraphsTitle: "How to get a Captain Candy franchise",
        listParagraphs:  [
            "We see franchising as a longstanding and mutually beneficial relationship, a bit like accepting another person into our business family. We therefore take great care to ensure our selection process is fair, thorough and prudent.",
            " For this reason anyone interested in applying is asked to complete a QUESTIONNAIRE to share preliminary (and non-binding) information with us, which allows us to ascertain whether our future collaboration is built on sufficiently solid foundations and has real prospects for success. We’ll get in touch after reading through your responses.",
            <Fragment><i> By sending your responses, you are automatically entered onto our database of people interested in working with us. We treat any information contained in the questionnaire in accordance with the General Data Protection Regulation (GDPR) and use it exclusively for the purpose of potential collaboration with you.</i></Fragment>,
        ],
        questionTitle: "QUESTIONNAIRE",
        inputs: [
            {
                id: "contactform_fullName",
                name: "contactform_fullName",
                label: "Name and surname*",
            },
            {
                id: "contactform_email",
                name: "contactform_email",
                label: "E-mail*",
            },
            {
                id: "contactform_phone",
                name: "contactform_phone",
                label: "Telephone number*",
                class: "inputphone",
                style: { display: "inline-block" },
            },
            {
                id: "contactform_text_1",
                name: "contactform_text_1",
                label: "Why do you want to work with Captain Candy?",
                textarea: true,
            },
            {
                id: "contactform_text_2",
                name: "contactform_text_2",
                label: "How did you find out about Captain Candy?",
                textarea: true,
            },
            {
                id: "contactform_text_3",
                name: "contactform_text_3",
                label: "What is your current professional experience?*",
                textarea: true,
            },
            {
                id: "file_upload",
                name: "file_upload",
                label: "Please attach a detailed CV*",
                button: "Browse"
            },
            {
                id: "contactform_text_4",
                name: "contactform_text_4",
                label: "Do you have any experience running a small business? If o what?*",
                textarea: true,
            },
            {
                id: "contactform_text_5",
                name: "contactform_text_5",
                label: "Do you work for any other brands?*",
                textarea: true,
            },
            {
                id: "contactform_text_6",
                name: "contactform_text_6",
                label: "Do you have any other commitments or things that would restrict you from this work? Have you signed a competition clause preventing you from working with a particular brand or concept?*",
                textarea: true,
            },
            {
                id: "contactform_text_7",
                name: "contactform_text_7",
                label: "Do you or have you (or anyone close to you) previously run a franchise?",
                textarea: true,
            },
            {
                id: "contactform_text_8",
                name: "contactform_text_8",
                label: "If you’ve never run your own business before, what is your motivation for starting one now?",
                textarea: true,
            },
            {
                id: "contactform_text_9",
                name: "contactform_text_9",
                label: "Do you have a clean criminal record and are you prepared to supply proof of this?*",
                textarea: true,
            },
            {
                id: "contactform_text_10",
                name: "contactform_text_10",
                label: "To what degree can you take part in the day to day running of the store?*",
                textarea: true,
            },
            {
                id: "contactform_text_11",
                name: "contactform_text_11",
                label: "What makes you think that Captain Candy will be a success in your city, country or region?",
                textarea: true,
            },
            {
                id: "contactform_text_12",
                name: "contactform_text_12",
                label: "Where do you want to open your premises? What town, big city, region or country interest you in terms of running Captain Candy franchising stores?*",
                textarea: true,
            },
            {
                id: "contactform_text_13",
                name: "contactform_text_13",
                label: "Do you have your own/rented sales spaces to run your Captain Candy store? How big is it, what are the floor plans and where is it? How long will you have it for?",
                textarea: true,
            },
            {
                id: "contactform_text_14",
                name: "contactform_text_14",
                label: "What level of funds are you prepared to invest in the Captain Candy project? How do you plan to finance construction, furnishings and running costs during the start-up phase? How much can you source from your own funds?*",
                textarea: true,
            },
        ],
        link: <Fragment>“I consent to the  <Link  onClick={() => toTop()} to="/personal-data-processing" className="link">processing of my personal data</Link> and the responses given in the questionnaire.</Fragment>,
        button: "Send"
    },
    nl: {
        title: "Captain Candy-franchising",
        history: [
            {
                question: "Vind je het concept van Captain Candy interessant?",
                answer: "Captain Candy is een Tsjechische snoepwinkel die een breed assortiment aan kant-en-klare snoepjes verkoopt. De eerste winkel werd in 2012 geopend in Praag, later breidde Captain Candy uit naar Italië en Nederland. Op dit moment hebben we meerdere winkels en we plannen nog meer te openen.",
            },
            {
                question: "Wil je de vreugde van een ontdekkingstocht en het enthousiasme om terug te keren naar de jeugd met je omgeving delen?",
                answer: "Ontdek wat een Captain Candy-franchise voor je kan betekenen en neem contact met ons op. De volgende stappen kunnen we het beste samen bespreken.",
            },
        ],
        list: [
            {
                headingtitle: "Waarom moet je ons kiezen als je franchise-partner?",
                headingLi: [
                    "Ons concept heeft zijn eigen unieke stijl",
                    "Wij bieden kwaliteit op internationaal niveau",
                    "We zijn constant bezig met het ontwikkelen en verbeteren van het concept",
                    "Wij zijn een bewezen Europees merk",
                ],
            },
            {
                headingtitle: "Wat bieden wij onze franchise-partners?",
                headingLi: [
                    "De zekerheid van succesvolle onderneming onder een internationaal merk",
                    "Bewezen kennis die continu wordt verbeterd",
                    "Goed bedachte, geteste, bewezen en kant-en-klare oplossing “op maat”",
                    "We bieden hulp niet alleen vóór de opening en in het begin, maar ook op elk moment daarna",
                    "Kans voor persoonlijke ontwikkeling en zelfontplooiing bij de lancering van je eigen winkel",
                    "Kans om deel te nemen aan de ontwikkeling en groei van het merk",
                    "Levering van de producten en inrichting die nodig is om de franchise te exploiteren",
                    "Systeem van regelmatige controles met overleg",
                    "Lange termijn inkomen en rendement op je investering",
                    "Onderlinge overeenstemming over een bepaalde vorm van samenwerking",
                ]
            }
        ],
        listParagraphsTitle: "Hoe krijg je een Captain Candy-franchise",
        listParagraphs:  [
            "We begrijpen franchising als een langdurige en wederzijds nuttige relatie, als het opnemen van een nieuw lid in onze zakenfamilie. Daarom beoordelen we de selectie eerlijk, zorgvuldig en welbewust.",
            "Met elke geïnteresseerde die ons benadert starten we eerst een voorlopige (en vrijblijvende) informatie-uitwisseling met behulp van een VRAGENLIJST om te ontdekken of onze eventuele samenwerking voldoende draagvlak en een realistisch perspectief zal hebben. Wij verwerken je antwoorden en komen er dan bij je op terug.",
            <Fragment><i>Door de antwoorden op te sturen, nemen we je ook automatisch op in de database van
            geïnteresseerden in samenwerking. Wij behandelen de informatie in de vragenlijst in
            overeenstemming met de Wet bescherming persoonsgegevens (AVG) en gebruiken deze alleen voor
            het tot stand brengen van samenwerking.</i></Fragment>,
        ],
        questionTitle: "VRAGENLIJST",
        inputs: [
            {
                id: "contactform_fullName",
                name: "contactform_fullName",
                label: "Voornaam en achternaam*",
            },
            {
                id: "contactform_email",
                name: "contactform_email",
                label: "E-mail*",
            },
            {
                id: "contactform_phone",
                name: "contactform_phone",
                label: "Telefoonnummer*",
                class: "inputphone",
                style: { display: "inline-block" },
            },
            {
                id: "contactform_text_1",
                name: "contactform_text_1",
                label: "Waarom wil je met Captain Candy samenwerken?",
                textarea: true,
            },
            {
                id: "contactform_text_2",
                name: "contactform_text_2",
                label: "Hoe heb je over Captain Candy gehoord?",
                textarea: true,
            },
            {
                id: "contactform_text_3",
                name: "contactform_text_3",
                label: "Wat is je vorige professionele ervaring?*",
                textarea: true,
            },
            {
                id: "file_upload",
                name: "file_upload",
                label: "Voeg een gestructureerd CV toe*",
                button: "Bladeren"
            },
            {
                id: "contactform_text_4",
                name: "contactform_text_4",
                label: "Heb je ervaring met het runnen van een retail-winkel? Zo ja, welke?*",
                textarea: true,
            },
            {
                id: "contactform_text_5",
                name: "contactform_text_5",
                label: "Werk je nog voor een ander merk?*",
                textarea: true,
            },
            {
                id: "contactform_text_6",
                name: "contactform_text_6",
                label: "Heb je verplichtingen, beperkingen of verbod op concurrentie opgelegd door een merk of concept?*",
                textarea: true,
            },
            {
                id: "contactform_text_7",
                name: "contactform_text_7",
                label: "Heb je of had je (of iemand in je omgeving) ooit een franchise?",
                textarea: true,
            },
            {
                id: "contactform_text_8",
                name: "contactform_text_8",
                label: "Als je nog nooit een onderneming had, was zijn nu je overwegingen om te beginnen?",
                textarea: true,
            },
            {
                id: "contactform_text_9",
                name: "contactform_text_9",
                label: "Heb je een schoon strafblad en ben je bereid om het te bewijzen?*",
                textarea: true,
            },
            {
                id: "contactform_text_10",
                name: "contactform_text_10",
                label: "In hoeverre kun je je actief inzetten voor het dagelijkse beheer van de winkel?*",
                textarea: true,
            },
            {
                id: "contactform_text_11",
                name: "contactform_text_11",
                label: "Waarom denk je dat Captain Candy zou kunnen slagen in jouw stad, regio of land?",
                textarea: true,
            },
            {
                id: "contactform_text_12",
                name: "contactform_text_12",
                label: "Waar wil je je winkel openen? In welke stad (steden), hoofdstad (hoofdsteden), regio(-s) of land(en) ben je geïnteresseerd wat het exploiteren van je Captain Candy-franchise betreft?*",
                textarea: true,
            },
            {
                id: "contactform_text_13",
                name: "contactform_text_13",
                label: "Heb je je eigen/gehuurde verkoopruimte tot je beschikking voor de Captain Candy winkel? Hoe groot, wat is de plattegrond en wat is de locatie ervan? Hoe lang zal je de ruimte tot je beschikking hebben?",
                textarea: true,
            },
            {
                id: "contactform_text_14",
                name: "contactform_text_14",
                label: "Hoeveel geld heb je beschikbaar voor het Captain Candy-project? Hoe ben je van plan de bouw, inrichting en exploitatie tijdens de opstart van de winkel te financieren? Wat voor eigen vermogen kan je in de onderneming investeren?*",
                textarea: true,
            },
        ],
        link: <Fragment>“Ik ga akkoord met de <Link  onClick={() => toTop()} to="/personal-data-processing" className="link">voorwaarden voor het verwerken van persoonsgegevens </Link>  en de antwoorden in deze vragenlijst.“</Fragment>,
        button: "VERZENDEN"
    },
};

export {
    franchisingData,
}