import { Fragment } from "react";
import { Link } from "react-router-dom";
const toTop = () => {
    window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
};

const career = {
    en: {
        title: "Jobs currently available with us",
        accordionTitle: "Sales assistant",
        accordionSubTitle: "Set off on a pirate’s adventure with help and us other people find their personal treasure trove! The world can never have enough reliable sailors on our ship and we’re prepared to pay them their weight in gold!",
        accordion: [
            {
                id: "1",
                title: "Sales assistant",
                content:  "Set off on a pirate’s adventure with help and us other people find their personal treasure trove! The world can never have enough reliable sailors on our ship and we’re prepared to pay them their weight in gold!",
                item: [
                    {
                        title: "Why sail with us?",
                        list: [
                            "Our boat is anchored right in the heart of Prague",
                            "No matter how big the wave, we can’t be overturned, we’re a stable Czech company",
                            "Our sailors get to work flexible shifts",
                            "There’s a friendly atmosphere among members of the crew ",
                            "We offer fair and motivating pay",
                            "Opportunity to earn extra pay as part of our bonus programme",
                            "Opportunities to climb the career ladder with us – from sailor all the way to admiral",
                            "Get to choose the length of your expedition  - full or part-time hours",
                            "We’ll train you up on everything you need to know to become a fully-fledged crew member",
                        ]
                    },
                    {
                        title: "What are our expectations once you’re on board?",
                        list: [
                            "Friendly and positive behaviour when communicating with the natives as well as foreigners",
                            "Willingness to learn new skills",
                            "Flexibility to weather all storms",
                            "To work with zeal and have a healthy work ethic.",
                        ]
                    },
                    {
                        title: "What kind of work awaits our eager new recruits?",
                        list: [
                            "To help and serve everyone the tide brings in",
                            "To keep the trunks and barrels fully stocked with sweet treats",
                            "To pop down to the lower decks to pick up supplies",
                            "To scrub the decks and captain’s bridge every now and then after a hard day’s work",
                        ],
                    },
                ],
                itemFooter: "If you’re interested and keen to get on board with us, be ready to set sail right away!",
                tabel: <Fragment>
                        <table>
                            <thead><tr><td></td><td></td></tr></thead>
                            <tbody>
                                <tr>
                                    <td>E-mail:</td>
                                    <td>
                                        <a href="mailto:join@captaincandy.cz"><b>join@captaincandy.cz</b></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mobile phone number:</td>
                                    <td><a href="tel:+420 731 147 082"><b>+420 731 147 082</b></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </Fragment>
            },
        ],
        itemFooter: "If you’re interested and keen to get on board with us, be ready to set sail right away!",
        inputs: [
            {
                id: "contactform_fullName",
                name: "contactform_fullName",
                label: "Name and surname",
            },
            {
                id: "contactform_email",
                name: "contactform_email",
                label: "E-mail",
            },
            {
                id: "contactform_phone",
                name: "contactform_phone",
                label: "Telephone number",
                class: "inputphone",
                style: { display: "inline-block" },
            },
            {
                id: "contactform_text",
                name: "contactform_text",
                label: "Covering letter",
                textarea: true,
            },
            {
                id: "cv",
                name: "cv",
                label: "CV",
                placeholder: "Browser",
                file: true,
            },
        ],
        link: <Fragment>“I consent to the <Link onClick={() => toTop()} to="/personal-data-processing" className="link">processing of my personal data</Link> and the responses given in the questionnaire.</Fragment>,
        button: "Send message"
    },
    nl: {
        accordion: [
            {
                id: "1",
                title: " Where do the sweets in Captain Candy’s stores come from?", 
                content: "Captain Candy’s ships travel all over Europe (from Spain to Germany, from Sweden to Italy) to the very best suppliers for candy. All you have to do is look at the barrel or the box and that will tell you where the particular candy has sailed from."
            },
        ],
        title: "Jobs currently available with us",
        accordionTitle: "Sales assistant",
        accordionSubTitle: "Set off on a pirate’s adventure with help and us other people find their personal treasure trove! The world can never have enough reliable sailors on our ship and we’re prepared to pay them their weight in gold!",
        item1: <Fragment>
                <div className="item-inner">
                    <h4 className="heading">
                        Why sail with us?
                    </h4>
                    <ul>
                        <li>Our boat is anchored right in the heart of Prague</li>
                        <li>No matter how big the wave, we can’t be overturned, we’re a stable Czech company</li>
                        <li>Our sailors get to work flexible shifts </li>
                        <li>There’s a friendly atmosphere among members of the crew </li>
                        <li>We offer fair and motivating pay </li>
                        <li>Opportunity to earn extra pay as part of our bonus programme </li>
                        <li>Opportunities to climb the career ladder with us – from sailor all the way to admiral</li>
                        <li> Get to choose the length of your expedition  - full or part-time hours</li>
                        <li>We’ll train you up on everything you need to know to become a fully-fledged crew member</li>
                    </ul>
                </div>
            </Fragment>,
        itemFooter: "If you’re interested and keen to get on board with us, be ready to set sail right away!",
        inputs: [
            {
                id: "contactform_fullName",
                name: "contactform_fullName",
                label: "Naam en achternaam",
            },
            {
                id: "contactform_email",
                name: "contactform_email",
                label: "E-mail",
            },
            {
                id: "contactform_phone",
                name: "contactform_phone",
                label: "Telefoon nummer",
                class: "inputphone",
                style: { display: "inline-block" },
            },
            {
                id: "contactform_text",
                name: "contactform_text",
                label: "Begeleidende brief",
                textarea: true,
            },
            {
                id: "cv",
                name: "cv",
                label: "cv",
                placeholder: "Bladeren",
                file: true,
            },
        ],
        link: <Fragment>“Ik stem in met de <Link  onClick={() => toTop()} to="/personal-data-processing" className="link">verwerking van mijn persoonsgegevens</Link> en de antwoorden op de vragenlijst.</Fragment>,
        button: "Bericht versturen"
    }
};

export {
    career,
};