import React from "react";
import hpBox from "../img/hp_box_1-min.jpg";
import hpBox1 from "../img/hp_box_2-min.jpg";
import hpBox2 from "../img/hp_box_3-min.jpg";
import hpBox3 from "../img/hp_box_4-min.jpg";

const boxes = {
    title: {
        en: "From Captain Candy’s board",
        nl: "Van het dek van Captain Candy",
    },
    en: [
        {
            img: hpBox,
            title: <React.Fragment>Does it stink?<br />It definitely does!</React.Fragment>,
            description: "Even when something stinks, it can still be good news.",
            link: "More information",
            href: "/products",
            longer: true,
        },
        {
            img: hpBox1,
            title: <React.Fragment>All the places we anchor</React.Fragment>,
            description: "You can now find us in four places in Prague!",
            link: "More information",
            href: "/products"
        },
        {
            img: hpBox2,
            title: <React.Fragment>The end of sour looks!</React.Fragment>,
            description: "Do people give you sour looks? Try offering them our sour jelly beans!",
            link: "More information",
            href: "/products",
            longer: true,
        },
        {
            img: hpBox3,
            title: <React.Fragment>Find out more about what we do</React.Fragment>,
            description: "Discover the journey of a Czech company that brings joy from all over the world.",
            link: "More information",
            href: "/products"
        },
    ],
    nl: [
        {
            img: hpBox,
            title: <React.Fragment>Stinkt het?<br />Jazeker!</React.Fragment>,
            description: "Zelfs als iets stinkt, kan het nog goed nieuws zijn",
            link: "Meer informatie",
            href: "/products",
            longer: true,
        },
        {
            img: hpBox1,
            title: <React.Fragment>Alle plaatsen waar we voor anker gaan</React.Fragment>,
            description: "Je kunt ons nu vinden op twee plekken in Praag.",
            link: "Meer informatie",
            href: "/products"
        },
        {
            img: hpBox2,
            title: <React.Fragment>Het einde van zure blikken!</React.Fragment>,
            description: "Kijken de mensen je zuur aan? Kijk eens of ze onze zure jelly beans willen proeven.",
            link: "Meer informatie",
            href: "/products",
            longer: true,
        },
        {
            img: hpBox3,
            title: <React.Fragment>Ontdek meer over wat we doen<br /></React.Fragment>,
            description: "Ontdek de reis van een Tsjechisch bedrijf dat je lekkers brengt uit heel de wereld.",
            link: "Meer informatie",
            href: "/products"
        },
    ]
}

const HpMapData = {
    en: {
        title: "Our store in Amsterdam",
        button: {
            name: "More information",
        }
    },
    nl: {
        title: "Onze winkel in Amsterdam",
        button: {
            name: "More information",
        }
    }
};
    
export { boxes, HpMapData };
    