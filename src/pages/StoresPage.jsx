import React, { useContext } from 'react';
import Stores from '../components/Stores/Stores';
import { Context } from '../context/Products';
import { StorsData } from "../assets/dummyData/StorsData";

const StoresPage = () => {
  const [{ lang }] = useContext(Context);
  return (
    <Stores data={StorsData[lang]}/>
  )
}

export default StoresPage;