import React, { useContext } from 'react';
import { faqData } from '../assets/dummyData/faq';
import { NewsLetterContentFaq } from '../assets/dummyData/NewsLetterData';
import Faq from '../components/Faq/Faq';
import NewLetter from '../components/NewLetter/NewLetter';
import { Context } from '../context/Products';

const FaqPage = () => {
  const [{ lang }] = useContext(Context);
  return (
    <div id="page-faq" className="page">
        <Faq data={faqData[lang]} title={faqData?.title[lang]} />
        <NewLetter hiddeImg data={NewsLetterContentFaq[lang]} />
    </div>
  )
}

export default FaqPage;