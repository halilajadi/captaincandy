import React, { useContext } from 'react';
import { homePageData } from '../assets/dummyData/homePage';
import { boxes, HpMapData } from '../assets/dummyData/HpBoxesData';

import Header from '../components/Header/Header';
import HpAbout from '../components/HpAbout/HpAbout';
import HpBoxes from '../components/HpBoxes/HpBoxes';
import HpMap from '../components/Hpmap/HpMap';
import { Context } from '../context/Products';

const HomePage = () => {
  const [{ lang }] = useContext(Context);
  return (
    <div id="page-hp" className="page">
        <Header />
        <HpAbout
          data={homePageData[lang]}
        />
        <HpBoxes
          boxes={boxes[lang]}
          title={boxes.title[lang]}
        />
        <div className="clearfix" />
        <HpMap data={HpMapData[lang]}/>
    </div>
  )
}

export default HomePage;
