import React, { useContext } from 'react';
import About from '../components/About/About';
import AnimationEffects from '../assets/Animations/hooks/AnimationEffects';
import NewLetter from '../components/NewLetter/NewLetter';
import { AboutData } from '../assets/dummyData/About';
import { Context } from '../context/Products';
import { NewsLetterContent } from '../assets/dummyData/NewsLetterData';

const AboutPage = () => {
    const [{ lang }] = useContext(Context);
    const { singleRefs } = AnimationEffects(['NewLetter'], [] ,'opacity', 0.2 , 0.12);

    return (
        <>
            <div id="page-about">
                <About aboutData={AboutData[lang]} footerText={AboutData.footerText[lang]} />
            </div>
            <div className="opacity-0" ref={singleRefs.elements.NewLetter.ref}>
                <NewLetter data={NewsLetterContent[lang]} />
            </div>
        </>
    )
}

export default AboutPage;