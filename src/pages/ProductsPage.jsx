import React, { useContext } from 'react';
import AnimationEffects from '../assets/Animations/hooks/AnimationEffects';
import { NewsLetterContent } from '../assets/dummyData/NewsLetterData';
import { productsItems } from '../assets/dummyData/ProductsPageData';
import NewLetter from '../components/NewLetter/NewLetter';
import Products from '../components/Products/Products';
import { Context } from '../context/Products';

const ProductsPage = () => {
    const [{ lang }] = useContext(Context);
    const { singleRefs } = AnimationEffects(['NewLetter'], [] ,'fadeInY', 0.4, 0.12);
    return (
        <div id="page-products" className="page fade-onload">
            <Products
                productsItems={productsItems[lang]}
                title={productsItems.headTitle[lang]}
                subTitle={productsItems.headSubTitle[lang]}
            />
            <div className="opacity-0" ref={singleRefs.elements.NewLetter.ref}>
                <NewLetter data={NewsLetterContent[lang]} />
            </div>
        </div>
    )
}

export default ProductsPage;
