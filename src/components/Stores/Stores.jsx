import React from 'react';
import prodejnyMapBg from "../../assets/img/stores/prodejny_map_bg-min.jpg";
import prodejny_map_path_praha from "../../assets/img/stores/prodejny_map_path_praha.svg";
import prodejny_map_path_city_praha from "../../assets/img/stores/prodejny_map_path_city_praha.svg";
import prodejnyMapPathCityPrahaXs from "../../assets/img/stores/prodejny_map_path_city_praha-xs.svg";
// import prodejny_map_path_dubrovnik from "../../assets/img/stores/prodejny_map_path_dubrovnik.svg";
// import prodejny_map_path_city_dubrovnik from "../../assets/img/stores/prodejny_map_path_city_dubrovnik.svg";
// import prodejny_map_path_city_dubrovnikXs from "../../assets/img/stores/prodejny_map_path_city_dubrovnik-xs.svg";
import prodejny_map_path_benatky from "../../assets/img/stores/prodejny_map_path_benatky.svg";
import prodejny_map_path_city_benatky from "../../assets/img/stores/prodejny_map_path_city_benatky.svg";
import prodejny_map_path_city_benatkyXs from "../../assets/img/stores/prodejny_map_path_city_benatky-xs.svg";
import { Link } from 'react-router-dom';
import AnimationEffects from '../../assets/Animations/hooks/AnimationEffects';

const Stores = ({ data }) => {

    const scroll = (id) => {
        const section = document.getElementById(id);
        section.scrollIntoView( { behavior: 'smooth'} );
    };
    const elementsAnimate = []
    data.forEach((el) => {
        el.places.forEach((e) => {
            elementsAnimate.push(e.img);
        })
    });
    const { singleRefs } = AnimationEffects(elementsAnimate, data, 'fadeInY', 0.08 , 0.12);
      
    return (
        <div id="page-prodejny" className="page">
            {/* </div><!-- className="fade-onload" --> */}
            <header>
                <img src={prodejnyMapBg} alt="Stores map" />
                <div className="map-wrap">
                    <div id="img-map">
                        <div className="text-center">
                         
                        </div>
                    </div>
                    <div className="path" id="path-praha">
                        <img src={prodejny_map_path_praha} alt="Praha" className="prodejny-map-path" />
                        <button className="map-button" onClick={() => scroll("prague")}>
                            <img src={prodejny_map_path_city_praha} alt="Praha" className="prodejny-map-path-city hidden-xs" />
                            <img src={prodejnyMapPathCityPrahaXs} alt="Praha" className="prodejny-map-path-city visible-xs" />
                        </button>
                    </div>
                    {/* <div className="path" id="path-dubrovnik">
                        <img src={prodejny_map_path_dubrovnik} alt="Dubrovník" className="prodejny-map-path" />
                        <a href="#dubrovnik">
                            <img src={prodejny_map_path_city_dubrovnik} alt="Dubrovník" className="prodejny-map-path-city hidden-xs" />
                            <img src={prodejny_map_path_city_dubrovnikXs} alt="Dubrovník" className="prodejny-map-path-city visible-xs" />
                                                </a>
                    </div> */}
                    <div className="path" id="path-benatky">
                        <img src={prodejny_map_path_benatky} alt="Benátky" className="prodejny-map-path" />
                        <button className="map-button" onClick={() => scroll("venecie")}>
                            <img src={prodejny_map_path_city_benatky} alt="Benátky" className="prodejny-map-path-city hidden-xs" />
                            <img src={prodejny_map_path_city_benatkyXs} alt="Benátky" className="prodejny-map-path-city visible-xs" />
                        </button>
                    </div>
                </div>
            </header>
            <section className="boxes" data-js="scroll_fullpage" data-scroll-js="normal_scroll_content_start" id="our-stores">
                <div className="container-custom" id="benatky">
                    {data?.map((store) => (
                        <div className="row-custom" id={store.id} key={store.id}>
                            <h1 className="heading-big aos-init aos-animate" data-aos="fade-up">
                                {store?.title}
                            </h1>
                            <div className="boxes-wrap">
                                {store?.places?.map((place, i) => (
                                    <div key={place?.img} className="box aos-init aos-animate opacity-0" data-aos="fade-up" data-aos-delay="200" ref={singleRefs.elements[place.img].ref}>
                                        <div className="inner">
                                            <div className="img-wrap">
                                                <img src={place?.img} alt= {place?.title} />
                                            </div>
                                            <div className="content">
                                                <h3 className="ttl">
                                                    {place.title}
                                                    {/* <!--<small>30 121 Benátky, Itálie</small>--> */}
                                                </h3>
                                                <table>
                                                    <tbody>
                                                        {place?.table?.map((e, i) => (
                                                            <tr key={`tabel${i}`}>
                                                                <td>{e?.open}</td>
                                                                <td>{e?.time}</td>
                                                            </tr>
                                                        ))}
                                                    </tbody>
                                                </table>
                                                <Link to="https://www.google.cz/maps/place/Salizada+S.+Giovanni+Grisostomo,+5803,+30121+Venezia+VE,+It%C3%A1lie/@45.4390141,12.3349899,17z/data=!3m1!4b1!4m5!3m4!1s0x477eb1dbee572bd1:0x87490efe04ea7997!8m2!3d45.4390141!4d12.3371786">{place?.linkName}</Link>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    ))}
                </div>
            </section>
        </div>
    )
}

export default Stores;