import React from 'react'

const Contact = ({ data }) => {
  return (
    <div id="page-kontakt" className="page">
        <header id="main"></header>
        <div className="content">
            <div className="container-custom">
                <h1 className="page-heading fade-onload">
                    {data?.title}
                </h1>
                <p className="fade-onload">
                    {data?.history}
                </p>
                <br />
                <p className="fade-onload">
                    {data?.historyLink}
                </p>
            </div>
            {/* <!-- form --> */}
            <h2 className="heading aos-init aos-animate" data-aos="fade-up" id="kontakt">
                {data?.questionTitle}
            </h2>
            <form id="contact_form" className="validate aos-init aos-animate">
                {data?.inputs?.map((element) => (
                    <div className="form-group has-feedback">
                        <label  className="textarea" htmlFor={element.id}>{element.label}</label>
                        <div className="berlicka">
                            {element?.textarea ? (
                                    <textarea name={element.name} id={element.id} className="form-control" rows="4"></textarea>
                                )
                                : (
                                    <input name={element.name} type="text" className={`${element?.class} form-control`} id={element.id} placeholder="" required="" />
                                )}
                            <span className="valid-icon"></span>
                        </div>
                    </div>
                ))}
                {/* <div className="form-group has-feedback">
                    <label for="i2">E-mail</label>
                    <div className="berlicka">
                        <input name="email" type="email" className="form-control" id="i2" placeholder="" data-contactform-delete="" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" />
                        <span className="valid-icon"></span>
                    </div>
                </div> */}
                {/* <!-- tel nepovinny --> */}
                {/* <div className="form-group has-feedback">
                    <label for="i3">Telephone number</label>
                    <div className="berlicka">
                        <input name="phone" type="text" className="form-control inputphone" id="i3" data-contactform-delete="" placeholder="" pattern="([0-9\+\s]){9,}" />
                        <span className="valid-icon"></span>
                    </div>
                </div>
                <div className="form-group">
                    <label for="t1" className="textarea">What is it?</label>
                    <div className="berlicka">
                        <textarea name="text" id="t1" className="form-control" rows="4" data-contactform-delete="" required=""></textarea>
                        <span className="valid-icon"></span>
                    </div>
                </div> */}
                <div className="clearfix"></div>
                <div className="form-group agree">
                    <label></label>
                    <div className="checkbox-wrap">
                        <input type="checkbox" id="checkbox" required="" />
                        <label for="checkbox" className="circle"></label>
                        <label for="checkbox" className="label">{data?.link}</label>
                    </div>
                </div>
                <button type="submit" className="btn btn-primary disabled">{data?.button}</button>
            </form>

        {/* <!-- uspesne odeslano -> nahrazuje formular (cely <form></form>) --> */}
        <div className="msg-send  hidden" id="contact_form_send_success">
            <span className="ttl">
                Successfully sent!
            </span>
            <p>
                A bottle with your message in it is already on its way to us.
            </p>
            <p>
                Fear not, no torture awaits, but unless you confirm your registration in the e-mail we just pinged your way, we’re under strict orders from the royal fleet to steer clear…
            </p>
            <button type="submit" className="btn btn-primary" data-contactform-resend="1">Send another</button>
        </div>

        <div className="msg-send not-send hidden" id="contact_form_send_error">
            <span className="ttl">
                E-mail was not sent
            </span>
            <p>
                Something went wrong, please contact us in another way
            </p>
            <button type="submit" className="btn btn-primary" data-contactform-resend="1">Try again</button>
        </div>
        </div>
    </div>
  )
}

export default Contact;