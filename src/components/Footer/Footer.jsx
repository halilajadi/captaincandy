import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { footerMenu } from '../../assets/dummyData/Menu';
import Logo from "../../assets/img/brand-logo.svg";
import { Context } from '../../context/Products';

const Footer = ({ activeClass }) => {
    const [{ lang }] = useContext(Context);
  return (
    <footer id="main">
        <div className="container-custom">
            <div className="left">
                <img src={Logo} alt="Captain Candy" className="brand-logo" />
                <span className="copyright hidden-sm hidden-md hidden-lg">
                    ©&nbsp;2022&nbsp;Captain&nbsp;Candy
                </span>
            </div>
            <ul>
                {footerMenu[lang].map((el) => (
                    <li
                        key={el.path} className={(activeClass === el.path) ? "active" : ""}
                    >
                        <Link
                            onClick={() => {
                                window.scrollTo({top: 0, left: 0, behavior: 'smooth'});
                            }}
                            to={el.path}
                        >
                            {el.name}
                        </Link>
                    </li>
                ))}
            </ul>
        </div>
    </footer>
  )
}

export default Footer;