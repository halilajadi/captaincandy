import React, { useState } from 'react';
import { validateEmail, validatePhone } from '../../assets/function';

const Franchising = ({ data }) => {
    const [agreement, setAgreement] = useState(false);
    // const [hasError, setHasError] = useState();
    const [file, setFile] = useState();
    const [required, setRequired] = useState(false);
    const [formData, setFormData] = useState({
        agreement: false,
        contactform_email: "",
        contactform_fullName: "",
        contactform_phone: "",
        contactform_text: "",
    })

    const onchange = (e) => {
        // console.log("formData", e.target.value);
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const validInputs = (e) => {
        if (e === "contactform_email") {
            const valid = validateEmail(formData.contactform_email);
            if (valid) {
                return false;
            } else {
                return true;
            }
        } else if (e === "contactform_phone") {
            const valid = validatePhone(formData.contactform_phone);
            if (valid) {
                return false;
            } else {
                return true;
            }
        } else {
            if (formData[e]?.length > 2) {
                return false;
            } else {
                return true;
            }
        }
    }

    const handleImageChange = (e) => {
        const files = e.target.files;
        if (files?.length) {
            const file = files[0];
            const ext = file.name.split('.').pop();
            if(ext === "pdf" || ext === "docx" || ext === "doc" || ext === "jpg" || ext === "jpeg"|| ext ===  "png"|| ext ===  "xml"){
                setFile(file);
            }
            else{
                alert("Not accepted format!");
            }
        }
    };
    const sendEmail = (e) => {
        e.preventDefault();
        // const valid = validateEmail(formData);
        if (agreement) {
            setRequired(false);
        } else {
            setRequired(true);
        }
    }
    return (
        <div id="page-faq" className="page page-plain fade-onload">
            <header id="main"></header>
            <div className="container-custom">
                <h1 className="page-heading">
                    {data?.title || "Captain Candy Franchising"}
                </h1>
                <div className="content-1400">
                    <div data-aos="fade-up">
                        {data?.history?.map((el, index) => (
                            <div key={index}>
                                <p><b>{el.question}</b></p>
                                <p><b>{el.answer}</b></p>
                            </div>
                        ))}
                    </div>
                    <div data-aos="fade-up">
                        {data?.list?.map((e, index) => (
                            <div key={`test${index}`}>
                                <h2 className="heading">
                                    {e?.headingtitle}
                                </h2>
                                <ul>
                                    {e?.headingLi?.map((el, i) => (
                                        <li key={`list-${i}`}>{el}</li>
                                    ))}
                                </ul>
                            </div>
                        ))}

                        <h2 className="heading">
                            {data?.listParagraphsTitle}
                        </h2>
                        {data?.listParagraphs?.map((element, index) => (
                            <p key={`form-${index}`}>{element}</p>
                        ))}
                    </div>
                </div>
                {/* <!-- form --> */}
                <h2 className="heading-contactform" data-aos="fade-up" id="kontakt">
                    {data?.questionTitle}
                </h2>
                {/* <!-- fransizing formular --> */}


                <form id="contact_form" onSubmit={sendEmail} className="validate full-width fade-up">
                    {data?.inputs?.map((input) => (
                        <div key={input.id}>
                        {input.id === "file_upload" && (
                            <>
                                <div className={`form-group relative upload-wrap ${(!file && required) && "has-error has-danger"}`}>
                                    <label className="no-margin" style={{ minHeight: "45px" }}>{input?.label}</label>
                                    <div className="clearfix"></div>
                                    <div className="spacer5 visible-xs"></div>
                                    <button className='btn'>
                                        {input?.button}...
                                        <input
                                            id={input.id}
                                            type="file"
                                            name={input.id}
                                            size="40"
                                            onChange={(e) => handleImageChange(e)}
                                        />
                                        <span className="valid-icon" style={{ top: "7px", right: "8px" }}></span>
                                    </button>
                                    {file && <span className="upload-file-info selected">{file.name}</span>}
                                </div>
                                <div className="clearfix"></div>
                                <div className="spacer30"></div>
                            </>
                        )}
                        {input.id !== "file_upload" && (
                            <div className={`form-group ${(required && (validInputs(input.name))) && "has-error has-danger"}`} key={input.id}>
                                <label className={input?.textarea ? "textarea" : " "} htmlFor={input.id}>{input.label}</label>
                                <div className="berlicka" style={input?.style || {} }>
                                    {input?.textarea ? (
                                        <textarea name={input.name} id={input.id} onChange={onchange} className="form-control" rows="4"></textarea>
                                    )
                                    : (
                                        <input name={input.name} type="text" onChange={onchange} className={`${input?.class} form-control`} id={input.id} placeholder="" required="" />
                                    )}
                                    <span className="valid-icon"></span>
                                </div>
                            </div>
                        )}
                        </div>
                    ))}
                    <div className="clearfix"></div>
                    <div className={`form-group agree ${(required && !agreement) && "has-error has-danger"}`}>
                        <label></label>
                        <div className="checkbox-wrap">
                            <input type="checkbox" id="checkbox" checked={agreement} name="agreement"  onChange={() => setAgreement(!agreement)} />
                            <label htmlFor="checkbox" className="circle"></label>
                            <label htmlFor="checkbox" className="label">{data?.link}</label>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <button type="submit" className="btn btn-primary">{data?.button}</button>
                </form>

                {/* <!-- uspesne odeslano -> nahrazuje formular (cely <form></form>) --> */}
                <div className="msg-send  hidden" id="contact_form_send_success">
                    <span className="ttl">
                        Successfully sent!
                    </span>
                    <p>
                        A bottle with your message in it is already on its way to us. As soon as we get your parchment, we will hurry to answer.
                    </p>
                    <button type="submit" className="btn btn-primary" data-contactform-resend="1">Send another</button>
                </div>
                <div className="msg-send not-send hidden" id="contact_form_send_error">
                    <span className="ttl">
                        E-mail was not sent
                    </span>
                    <p>
                        Something went wrong, please contact us in another way
                    </p>
                    <button type="submit" className="btn btn-primary" data-contactform-resend="1">Try again</button>
                </div>
                <div className="clearfix"></div>
                <div className="spacer70"></div>
                <div className="spacer50"></div>
            </div>
        </div>
    )
};

export default Franchising;