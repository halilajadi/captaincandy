import React, { useState }from 'react';
import { validateEmail } from '../../assets/function';
import newsletterRope from "../../assets/img/newsletter-rope.png";
// import novinkyNaEmailEn from "../../assets/img/novinky-na-email_en.svg";
// import FlessenpostOntvangen from "../../assets/img/Flessenpost-ontvangen.svg";

const NewLetter = ({ hiddeImg, data }) => {
    const [formData, setFormData] = useState();
    const [hasError, setHasError] = useState();
    const [required, setRequired] = useState(false);
    const onchange = (e) => {
        setFormData(e.target.value);
        if (required) {
            const valid = validateEmail(e.target.value);
            if (valid) {
                setHasError(false);
            } else {
                setHasError(true);
            }
        }
    }
    const sendEmail = (e) => {
        e.preventDefault();
        const valid = validateEmail(formData);
        if (valid) {
            setHasError(false);
            setRequired(false)
        } else {
            setRequired(true)
            setHasError(true);
        }
    }
  return (
    <div id="newsletter" className={`${!hiddeImg && "type-2"}`}>
        {!hiddeImg && <img src={newsletterRope} alt="rope" className="hr-newsletter" />}
        <div className="container-custom">
            <div className="inner">
                <img src={data?.img} alt="Want to be first" />
                <p>
                    {data?.title}
                </p>
                <div className="component-newsletter">
                    <form className="input-wrap" onSubmit={(e) => sendEmail(e)}>
                        <div className="input-group has-feedback">
                            <input type="text" className="form-control" id="newsletter_input" onChange={onchange} placeholder={data?.placeholder} />
                            <span className="input-group-btn">
                                <button type="submit" className="btn btn-primary" id="newsletter_btn" data-newsletter-register-btn="">{data?.buttonName}</button>
                            </span>
                        </div>
                    </form>
                    {/* <!-- IF error, ELSE open #modal-email-success bud pres JS(https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp) nebo pres btn viz nahore --> */}
                    <span className={`error text-red ${!hasError && "hidden"}`} data-newsletter-registered-error="">
                        {data?.error}
                    </span>
                </div>
            </div>
        </div>
    </div>
  )
}

export default NewLetter;