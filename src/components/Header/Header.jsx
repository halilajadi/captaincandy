import React from 'react';
import Slider from "react-slick";

import Logo from "../../assets/img/brand-logo.svg";
import ArrowDown from "../../assets/img/arrow-down.svg";
import baner1 from "../../assets/img/slider/banner1.jpg";
import baner2 from "../../assets/img/slider/banner2.jpg";
import baner3 from "../../assets/img/slider/banner3.jpg";
import promoText from "../../assets/img/promo_text-sm_en.svg";
import promoTextXs from "../../assets/img/promo_text-xs_en.svg";
// import promoTextXsNl from "../../assets/img/nl-welcome-xs.svg";
import promoTextXsNl from "../../assets/img/web/welcome-nl-xs.svg";
import promoTextNl from "../../assets/img/web/welcome-nl.svg";
import "./style.css";
import { useContext } from 'react';
import { Context } from '../../context/Products';

const Header = () => {
    const [{ lang }] = useContext(Context);

    const settings = {
        draggable: false,
        arrows: false,
        dots: false,
        fade: true,
        speed: 2000,
        infinite: true,
        cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        autoplay: 1,
        autoplaySpeed: 4000,
        pauseOnHover: false
    };

    const handleClick = () => {
        const pageHeight = window.innerHeight;
        window.scrollTo({top: pageHeight, left: 0, behavior: 'smooth'})
    };

    return (
        <header id="main" className="scroll-fullpage" data-hash="a">
            <img src={Logo} alt="Captain Candy" id="brand-logo" />
            <img src={ArrowDown} alt="more" id="arrow-down" onClick={() => handleClick()} />
            <h1 id="motto-main">
                <span id="promo1" className="hidden-xs hidden-sm hidden-sm-plus" style={{ display: "block" }}>
                    · Welcome on varis board ·
                </span>
                <div className="visible-xs">
                    {lang === "en" ?  <img src={promoTextXs} alt="Welcome on varis board" id="promo-xs" /> :
                    <img src={promoTextXsNl} alt="Welcome on varis board" id="promo-xs" /> }
                </div>
                <div className="visible-sm visible-sm-plus">
                    {lang === "en" ? <img src={promoText} alt="Welcome on varis board" id="promo-sm" /> :
                    <img src={promoTextNl} alt="Welcome on varis board" id="promo-sm" />}
                </div>
            </h1>
            <div className="slideshow">
                <Slider {...settings}>
                    <div className="item"><img src={baner1} srcSet={baner1} alt="img" /></div>
                    <div className="item"><img src={baner2} srcSet={baner2} alt="img" /></div>
                    <div className="item"><img src={baner3} srcSet={baner3} alt="img" /></div>
                </Slider>
            </div>
        </header>
    )
}

export default Header