import React, { useState, useEffect, useContext } from 'react';
import { newsletter } from '../../assets/dummyData/popupNewsletter';
import { validateEmail } from '../../assets/function';
import { Context } from '../../context/Products';

const PopupNewsletter = () => {
    const [{ lang }] = useContext(Context);
    const [test, settest] = useState(true);
    const [loading, setLoading] = useState(false);
    const [formData, setFormData] = useState();
    const [hasError, setHasError] = useState();
    const [required, setRequired] = useState(false);
    const body = document.body;

    const closeModal = (event) => {
        if (event.target.id === "modal-popup-newsletter" || event.target.id === "modal-popup-newsletter-span") {
            body.classList.remove("modal-open");
            setLoading(true)
        }
    }
    const onchange = (e) => {
        setFormData(e.target.value);
        if (required) {
            const valid = validateEmail(e.target.value);
            if (valid) {
                setHasError(false);
            } else {
                setHasError(true);
            }
        }
    }
    const sendEmail = (e) => {
        e.preventDefault();
        const valid = validateEmail(formData);
        if (valid) {
            setHasError(false);
            setRequired(false)
        } else {
            setRequired(true)
            setHasError(true);
        }
    }
    if (test) {
        body.classList.add("modal-open");
        settest(false)
    }
    useEffect(() => {
        setLoading(false);
        // setClassList();
    }, [])
    return (
        <div id="modal-popup-newsletter" className="modal in" role="dialog" style={loading ? { display: "none" } : { display: "block" }} onClick={(e) => closeModal(e)}>
            <div className="modal-dialog">
                {/* <!-- Modal content--> */}
                <div className="modal-content on-click">
                    <span className="close" id="modal-popup-newsletter-span" data-dismiss="modal" onClick={(e) => closeModal(e)}></span>
                    <div className="modal-body">
                        <div className="content">
                            <div className="inner">
                                <span className="ttl">{newsletter[lang].title}</span>
                                <span className="subttl">{newsletter[lang].subttl}</span>
                                <div className="component-newsletter">
                                    <form className="input-wrap" onSubmit={(e) => sendEmail(e)}>
                                        <div className="input-group has-feedback">
                                            {/* <input type="text" className="form-control" id="newsletter_input" onChange={onchange} placeholder={newsletter[lang].placeholder} pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" /> */}
                                            <input type="text" className="form-control" id="newsletter_input" onChange={onchange} placeholder={newsletter[lang].placeholder} />
                                            <span className="input-group-btn">
                                                <button type="submit" className="btn btn-primary" id="newsletter_btn" data-newsletter-register-btn="">{newsletter[lang].button}</button>
                                            </span>
                                        </div>
                                    </form>
                                    {/* <!-- IF error, ELSE open #modal-email-success bud pres JS(https://www.w3schools.com/bootstrap/bootstrap_ref_js_modal.asp) nebo pres btn viz nahore --> */}
                                    <span className={`error text-red ${!hasError && "hidden"}`} data-newsletter-registered-error="">
                                        {newsletter[lang].error}  
                                    </span>
                                    {/* <!-- /IF error --> */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default PopupNewsletter;