import React, { useState } from 'react';
import NewBook from '../Book/NewBook';
import "./style.css";

const CaptainStory = ({ data }) => {
    const [pageNumber, setPageNumber] = useState(0);
    const checkWidth = (window.innerWidth > 767);

    const onFlipData = (data) => {
        setPageNumber(data);
        console.log("aaa", data)
    }

    return (
        <div id="page-story" className="fade-onload">
            <header>
                <div id="book-bg">
                    <div id="mybook" className="booklet position-relative">
                        <div className='position-relative w-100 h-100'>
                            {checkWidth ? (
                                <NewBook data={data} onFlipData={(e) => onFlipData(e)} />
                            ) : (
                                data?.map((el, index) => (
                                el.data
                                ))
                            )}
                        </div>
                    </div>
                    {/* {checkWidth && (
                        <div className="b-controls">
                            {pageNumber > 0 && (
                                <div
                                    className="b-tab b-tab-prev b-prev"
                                    title=""
                                    style={{ width: "60px", height: "20px", top: "-20px", cursor: "default" }}
                                    onClick={() => book.current.pageFlip().flipPrev()}
                                />
                            )}
                            {(pageNumber < (data?.length - 2)) && (
                                <div
                                    className="b-tab b-tab-next b-next"
                                    title=""
                                    onClick={() => next()}
                                    style={{ width: "60px", height: "20px", top: "-20px", cursor: "pointer"}}
                                />
                            )
                            }
                        </div>
                    )} */}
                </div>
            </header>
        </div>
    )
}

export default CaptainStory;