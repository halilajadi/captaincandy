import React from 'react';
import { useState } from 'react';
import { validateEmail, validatePhone } from '../../assets/function';
import Accordion from '../Accordion/Accordion';
import './style.css';

const Career = ({ data }) => {
    const [agreement, setAgreement] = useState(false);
    // const [hasError, setHasError] = useState();
    const [file, setFile] = useState();
    const [required, setRequired] = useState(false);
    const [formData, setFormData] = useState({
        agreement: false,
        contactform_email: "",
        contactform_fullName: "",
        contactform_phone: "",
        contactform_text: "",
    })

    const onchange = (e) => {
        // console.log("formData", e.target.value);
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const validInputs = (e) => {
        if (e === "contactform_email") {
            const valid = validateEmail(formData.contactform_email);
            if (valid) {
                return false;
            } else {
                return true;
            }
        } else if (e === "contactform_phone") {
            const valid = validatePhone(formData.contactform_phone);
            if (valid) {
                return false;
            } else {
                return true;
            }
        } else {
            if (formData[e]?.length > 2) {
                return false;
            } else {
                return true;
            }
        }
    }
    const handleImageChange = (e) => {
        const files = e.target.files;
        if (files?.length) {
            const file = files[0];
            const ext = file.name.split('.').pop();
            if(ext === "pdf" || ext === "docx" || ext === "doc" || ext === "jpg" || ext === "jpeg"|| ext ===  "png"|| ext ===  "xml"){
                setFile(file);
            }
            else{
                alert("Not accepted format!");
            }
        }
    };
    const sendEmail = (e) => {
        e.preventDefault();
        // const valid = validateEmail(formData);
        if (agreement) {
            setRequired(false);
        } else {
            setRequired(true);
        }
    }

  return (
    <div id="page-kariera" className="page">
        <header id="main"></header>
        <div className="container-custom" id="page-faq">
            <h1 className="page-heading fade-onload">
                {data?.title}
            </h1>
            <Accordion data={data?.accordion} />
            <div className="content-1200">
                <h2 className="heading aos-init aos-animate" data-aos="fade-up">
                    If you’re interested and keen to get on board with us, be ready to set sail right away!
                </h2>
            </div>
            <form id="contact_form" onSubmit={sendEmail} className="validate aos-init aos-animate">
                {data?.inputs?.map((element) => (
                    <div key={element.id} className={`form-group ${(required && (validInputs(element.name))) && "has-error has-danger"}`}>
                        {!element?.file && <label  className="textarea" htmlFor={element.id}>{element.label}</label>}
                        {element?.file ? (
                            <div className="form-group relative upload-wrap">
                                <label>{element.label}</label>
                                <button className="btn">
                                    {element.placeholder}...
                                    <input id="file_upload" type="file" name="file_1" size="40" onChange={(e) => handleImageChange(e)} />
                                </button>
                                {file && <span className="upload-file-info selected">{file.name}</span>}
                            </div>
                        ) : (
                            <div className="berlicka">
                                {element?.textarea ? (
                                    <textarea name={element.name} id={element.id} onChange={onchange}  className="form-control" rows="4"></textarea>
                                )
                                : (
                                    <input name={element.name} type="text"  onChange={onchange}  className={`${element?.class} form-control`} id={element.id} placeholder="" required="" />
                                )}
                                <span className="valid-icon"></span>
                            </div>
                        )}
                        
                    </div>
                ))}
                {/* <div className="form-group relative upload-wrap">
                    <label>CV</label>
                    <a className="btn" href="javascript:;">
                        Browse…
                        <input id="file_upload" type="file" name="file_1" size="40" accept=".jpg,.jpeg,.png,.pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" onChange={() => {}} />
                    </a>
                    <span className="upload-file-info"></span>
                </div> */}

                <div className="clearfix"></div>
                <div className={`form-group agree ${(required && !agreement) && "has-error has-danger"}`}>
                    <label></label>
                    <div className="checkbox-wrap">
                        <input type="checkbox" id="checkbox"  checked={agreement} name="agreement"  onChange={() => setAgreement(!agreement)} />
                        <label htmlFor="checkbox" className="circle"></label>
                        <label htmlFor="checkbox" className="label">{data?.link}</label>
                    </div>
                </div>
                <div className="clearfix"></div>
                <button type="submit" className="btn btn-primary disabled">{data?.button}</button>
            </form>

            {/* <!-- uspesne odeslano -> nahrazuje formular (cely <form></form>) --> */}
            <div className="msg-send  hidden" id="contact_form_send_success">
                <span className="ttl">
                    Successfully sent!
                </span>
                <p>
                    A bottle with your message in it is already on its way to us. As soon as we get your parchment, we will hurry to answer.
                </p>
                <button type="submit" className="btn btn-primary" data-contactform-resend="1">Send another</button>
            </div>

            <div className="msg-send not-send hidden" id="contact_form_send_error">
                <span className="ttl">
                    E-mail was not sent
                </span>
                <p>
                    Something went wrong, please contact us in another way
                </p>
                <button type="submit" className="btn btn-primary" data-contactform-resend="1">Try again</button>
            </div>
        </div>
    </div>
  )
}

export default Career;