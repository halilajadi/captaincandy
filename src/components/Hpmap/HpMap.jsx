import React from 'react';
import { useNavigate } from 'react-router-dom';

import Logo from "../../assets/img/brand-logo.svg";
import Maps from './Maps';

const HpMap = ({ data }) => {
    const navigate = useNavigate();
    
    const goToURL = (e) => {
        const ridirect = `/stores#${e}`;
        navigate(ridirect);
    }

    // useEffect(() => {
    //     const script = document.createElement("script");
        
    //     script.src = "maps/api/js?key=AIzaSyDLKc_wA4t1eOykcIJcs0kd0frcr2sYRvs&amp;sensor=false";
    //     // script.src = "https://use.typekit.net/foobar.js";
    //     script.async = true;
    
    //     document.body.appendChild(script);
    // }, []);
  return (
    <section id="map" className="fade-onload" data-js="scroll_fullpage">
        <div className="container-custom">
            <span className="hidden-xs hidden-sm">
                <img src={Logo} alt="Captain Candy" id="map-logo" /> 
            </span>
            <h2 className="section-heading">
                {data?.title}
            </h2>
            <div className="map-navigation hidden-xs hidden-sm" data-aos="fade-left">
                {/* <div className="item item-map-1 cp" onmouseover="hover(1)" onmouseout="out(1)" onclick="goToURL(); return false;"> */}
                <div className="item item-map-1 cp" onClick={() => goToURL("our-stores")}>
                    <h3 className="ttl">
                        Salizada San Giovanni Grisostomo 5803
                    </h3>
                    <div className="table-wrap">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Monday - Thursday</td>
                                    <td>10:00—22:00</td>
                                </tr>
                                <tr>
                                    <td>Friday - Saturday</td>
                                    <td>10:00—23:00</td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td>10:00—23:00</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr />
                    </div>
                </div>
                <div className="item item-map-2 cp" onClick={() => goToURL("our-stores")}>
                    <h3 className="ttl">
                        San Marco 5037
                    </h3>
                    <div className="table-wrap">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Monday - Thursday</td>
                                    <td>10:00—22:00</td>
                                </tr>
                                <tr>
                                    <td>Friday - Saturday</td>
                                    <td>10:00—23:00</td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td>10:00—22:00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="item item-map-3 cp" onClick={() => goToURL("our-stores")}>
                    <h3 className="ttl">
                        Castello 5577/5578
                    </h3>
                    <div className="table-wrap">
                        <table>
                            <tbody>
                                <tr>
                                    <td>Monday - Thursday</td>
                                    <td>10:00—22:00</td>
                                </tr>
                                <tr>
                                    <td>Friday - Saturday</td>
                                    <td>10:00—23:00</td>
                                </tr>
                                <tr>
                                    <td>Sunday</td>
                                    <td>10:00—22:00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {/* <iframe src="//snazzymaps.com/embed/22228" style={{ border: "none" }}></iframe> */}

            <div id="map_div-wrap" className="hidden-xs hidden-sm">
                <a href="/prodejny#our-stores" className="btn btn-primary btn-arrow btn-more-info">{data?.button?.name}</a>
                {/* <div id="map_div">
                </div> */}
                <Maps />
            </div>
        </div>
    </section>
  )
}

export default HpMap;
