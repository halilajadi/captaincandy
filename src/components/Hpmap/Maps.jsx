import React from 'react';
import "./style.css";
import { mapStyles } from './map';
// import img from "../../assets/img/map/transparent.png"
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '100%'
};


const center = {
  lat: 45.4361703, 
  lng: 12.335591,
};

function Maps() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyDLKc_wA4t1eOykcIJcs0kd0frcr2sYRvs&amp"
  })

  const [map, setMap] = React.useState(null)

  const onLoad = React.useCallback(function callback(map) {
    // This is just an example of getting and using the map instance!!! don't just blindly copy!
    const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds);

    setMap(map)
  }, []);
  // console.log("map", map);

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  return isLoaded ? (
      <GoogleMap
        disableDefaultUI={true}
        mapContainerStyle={containerStyle}
        center={center}
        zoom={16}
        onLoad={onLoad}
        onUnmount={onUnmount}
        options={{
          styles: mapStyles,
        }}
      >
        { /* Child components, such as markers, info windows, etc. */ }
        {/* <div title="Salizada San Giovanni Grisostomo 5803" aria-label="Salizada San Giovanni Grisostomo 5803" role="img" tabindex="-1" style="width: 66px; height: 73px; overflow: hidden; position: absolute; cursor: pointer; touch-action: none; left: -96px; top: -201px; z-index: -128;"><img alt="" src="https://maps.gstatic.com/mapfiles/transparent.png" draggable="false" style="width: 66px; height: 73px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div> */}
        <>
        <Marker
          position={{ lat: 45.439049, lng: 12.337171 }}
          onClick={()=> console.log("hike.name")}    
          title="Salizada San Giovanni Grisostomo 5803"
          name="My Marker"
          labelClass="blue"
          // icon={img}
        />
          {/* <div title="Salizada San Giovanni Grisostomo 5803" ariaLabel="Salizada San Giovanni Grisostomo 5803" role="img" tabindex="-1" style={{ width: "66px", height: "73px", overflow: "hidden", position: "absolute", cursor: "pointer", touchAction: "none", left: "-96px", top: "-201px", zIndex: "-128" }}><img alt="" src="https://maps.gstatic.com/mapfiles/transparent.png" draggable="false" style={{ width: "66px", height: "73px", userSelect: "none", border: "0px", padding: "0px", margin: "0px", maxWidth: "none"}} /></div> */}
          {/* </Marker> */}
        <Marker
          onClick={()=> console.log("hike.nam  e")}    
          position={{ lat: 45.436298, lng: 12.335698 }}
          // styles={
          // }
          />
        <Marker
          onClick={()=> console.log("hike.name 111")}    
          position={{ lat: 45.437462, lng: 12.338421 }}
          />
        </>

      </GoogleMap>
  ) : <></>
}

export default Maps;